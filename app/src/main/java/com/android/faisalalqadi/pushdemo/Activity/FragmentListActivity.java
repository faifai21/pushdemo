package com.android.faisalalqadi.pushdemo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.widget.Toast;

import com.android.faisalalqadi.pushdemo.Content.MasterDetailContent;
import com.android.faisalalqadi.pushdemo.Fragment.DummyContentFragment;
import com.android.faisalalqadi.pushdemo.Fragment.FragmentListFragment;
import com.android.faisalalqadi.pushdemo.Fragment.QrResultFragment;
import com.android.faisalalqadi.pushdemo.R;
import com.android.faisalalqadi.pushdemo.ZXing.IntentIntegrator;
import com.android.faisalalqadi.pushdemo.ZXing.IntentResult;


/**
 * An activity representing a list of Fragments. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link FragmentDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link com.android.faisalalqadi.pushdemo.Fragment.FragmentListFragment} and the item details
 * (if present) is a {@link com.android.faisalalqadi.pushdemo.Fragment.DummyContentFragment}.
 * <p>
 * This activity also implements the required
 * {@link com.android.faisalalqadi.pushdemo.Fragment.FragmentListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class FragmentListActivity extends Activity
        implements FragmentListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private String qrResultString = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_list);

        if (findViewById(R.id.fragment_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((FragmentListFragment) getFragmentManager()
                    .findFragmentById(R.id.fragment_list))
                    .setActivateOnItemClick(true);
        }

        // TODO: If exposing deep links into your app, handle intents here.
    }

    /**
     * Callback method from {@link FragmentListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(MasterDetailContent.ContentId id) {
        if(id == MasterDetailContent.ContentId.CAMERA ){
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.initiateScan();
            return;
        }
        String qrResult;
        if(qrResultString.isEmpty())
            qrResult = "Please open the camera and scan a QR code";
        else
            qrResult = qrResultString;

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            if(id == MasterDetailContent.ContentId.DUMMY)
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_detail_container, DummyContentFragment.newInstance(id))
                        .commit();
            else
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_detail_container, QrResultFragment.newInstance(qrResult))
                        .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.

            Intent detailIntent = new Intent(this, FragmentDetailActivity.class);

            detailIntent.putExtra(DummyContentFragment.ARG_ITEM_ID, id);

            if(id == MasterDetailContent.ContentId.QRRESULT)
                detailIntent.putExtra(QrResultFragment.ARG_QR_RESULT, qrResult);

            startActivity(detailIntent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            qrResultString = scanResult.getContents();
            onItemSelected(MasterDetailContent.ContentId.QRRESULT);
            Toast.makeText(this, scanResult.getContents(), Toast.LENGTH_SHORT).show();
        }
    }
}
