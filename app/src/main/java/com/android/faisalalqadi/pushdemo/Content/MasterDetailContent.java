package com.android.faisalalqadi.pushdemo.Content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class MasterDetailContent {

    public enum ContentId {
        DUMMY,
        CAMERA,
        QRRESULT
    }
    /**
     * An array of sample (dummy) items.
     */
    public static List<ContentItem> ITEMS = new ArrayList<ContentItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<ContentId, ContentItem> ITEM_MAP = new HashMap<ContentId, ContentItem>();

    static {
        // Add 3 sample items.
        addItem(new ContentItem(ContentId.DUMMY, "Dummy Fragment 1"));
        addItem(new ContentItem(ContentId.DUMMY, "Dummy Fragment 2"));
        addItem(new ContentItem(ContentId.CAMERA, "QR Scanner"));
        addItem(new ContentItem(ContentId.QRRESULT, "QR Result"));
    }

    private static void addItem(ContentItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class ContentItem {
        public ContentId id;
        public String content;

        public ContentItem(ContentId id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
