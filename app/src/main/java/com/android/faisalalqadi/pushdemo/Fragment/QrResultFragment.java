package com.android.faisalalqadi.pushdemo.Fragment;



import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.faisalalqadi.pushdemo.R;
import com.android.faisalalqadi.pushdemo.Request.GetRequest;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.springframework.web.client.HttpClientErrorException;
import org.w3c.dom.Text;

import roboguice.util.temp.Ln;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link QrResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class QrResultFragment extends Fragment implements RequestListener<String>{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_QR_RESULT = "qr-result";
    private SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
    private String qrResult;

    private TextView networkResult;
    private ProgressBar mProgressView;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param qrResult Parameter 1.
     * @return A new instance of fragment QrResultFragment.
     */
    public static QrResultFragment newInstance(String qrResult) {
        QrResultFragment fragment = new QrResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_QR_RESULT, qrResult);
        fragment.setArguments(args);
        return fragment;
    }
    public QrResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            qrResult = getArguments().getString(ARG_QR_RESULT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_qr_result, container, false);
        TextView qrResultText = (TextView) view.findViewById(R.id.qr_result);
        networkResult = (TextView) view.findViewById(R.id.network_result);
        mProgressView = (ProgressBar) view.findViewById(R.id.network_progress);
        showProgress(true);
        qrResultText.setText(qrResult);
        GetRequest req = new GetRequest(qrResult);
        spiceManager.execute(req, this);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(getActivity());
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        showProgress(false);
        if(spiceException.getCause() instanceof HttpClientErrorException){
            HttpClientErrorException e = (HttpClientErrorException) spiceException.getCause();
            Ln.e(e.getResponseBodyAsString(), "");

            networkResult.setText(e.getResponseBodyAsString());
        }
        else if(spiceException.getCause() instanceof IllegalArgumentException) {
            IllegalArgumentException e = (IllegalArgumentException) spiceException.getCause();
            networkResult.setText(e.getLocalizedMessage());
        }
    }

    @Override
    public void onRequestSuccess(String s) {
        showProgress(false);
        networkResult.setText(s);
    }

    private final void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            networkResult.setVisibility(show ? View.GONE : View.VISIBLE);
            networkResult.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    networkResult.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
        else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            networkResult.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
