package com.android.faisalalqadi.pushdemo.Fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.faisalalqadi.pushdemo.Content.MasterDetailContent;
import com.android.faisalalqadi.pushdemo.R;
import com.android.faisalalqadi.pushdemo.ZXing.IntentIntegrator;

/**
 * A fragment representing a single Fragment detail screen.
 * This fragment is either contained in a {@link com.android.faisalalqadi.pushdemo.Activity.FragmentListActivity}
 * in two-pane mode (on tablets) or a {@link com.android.faisalalqadi.pushdemo.Activity.FragmentDetailActivity}
 * on handsets.
 */
public class DummyContentFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private MasterDetailContent.ContentItem mItem;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id Parameter 1.
     * @return A new instance of fragment QrResultFragment.
     */
    public static DummyContentFragment newInstance(MasterDetailContent.ContentId id) {
        DummyContentFragment fragment = new DummyContentFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_ID, id);
        fragment.setArguments(args);
        return fragment;
    }
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DummyContentFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = MasterDetailContent.ITEM_MAP.get(getArguments().getSerializable(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.fragment_detail)).setText(mItem.content);
        }
        if(mItem.id == MasterDetailContent.ContentId.CAMERA ){
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.initiateScan();
        }
        return rootView;
    }
}
