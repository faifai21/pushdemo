package com.android.faisalalqadi.pushdemo.Request;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.octo.android.robospice.retry.RetryPolicy;

/**
 * Created by Fai on 2014-08-26.
 */
public class GetRequest extends SpringAndroidSpiceRequest<String> {
    private String url;

    public GetRequest(String url) {
        super(String.class);
        this.url = url;
    }

    @Override
    public String loadDataFromNetwork() throws Exception {
        // We know we're unauthinticated, so we're going to attempt this network call once, with no retries
        setRetryPolicy(new RetryPolicy() {
            @Override
            public int getRetryCount() {
                return 0;
            }

            @Override
            public void retry(SpiceException e) {

            }

            @Override
            public long getDelayBeforeRetry() {
                return 200;
            }
        });
        return getRestTemplate().getForObject(url, String.class);
    }
}
